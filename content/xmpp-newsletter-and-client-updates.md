Title: XMPP newsletter 2019-10 and client updates
Slug: xmpp-newsletter-and-client-updates
Date: 2019-10-04 13:00
Author: Martin
Tags: debian, xmpp, newsletter, profanity, gajim
Status: draft

The debian XMPP team is glad to inform that a new [XMPP newsletter][1],
containing articles, events and software releases, is out.

Some XMPP developers met in the cold north (Stockholm) for a [XMPP sprint][4],
working on improving a new groupchat bookmarks specification, file transfer 
interoperability issues, and a future landing page for new XMPP users.

There have also been XMPP related updates in debian since the last blog post:

* [Dino 0.0.git20190916.f746ce7-1][5] is now available in testing (bullseye)
* [Gajim 1.1.3][3] is now available in testing (bullseye)
* [Profanity 0.7.1][2] is now available in testing (bullseye)
  
[1]:https://XMPP.org/2019/10/newsletter-01-october/
[2]:https://packages.debian.org/bullseye/profanity
[3]:https://packages.debian.org/bullseye/gajim
[4]:https://bouah.net/2019/10/sprint-in-the-cold-north/
[5]:https://packages.debian.org/bullseye/dino-im
